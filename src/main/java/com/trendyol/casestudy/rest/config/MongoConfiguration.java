package com.trendyol.casestudy.rest.config;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.trendyol.casestudy.rest.Constants;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories(basePackages = "com.trendyol.casestudy.rest.repository")
public class MongoConfiguration extends AbstractMongoConfiguration {

    /**
     * Return the name of the database to connect to.
     *
     * @return must not be {@literal null}.
     */
    @Override
    protected String getDatabaseName() {
        return Constants.MONGO_DATABASE_NAME;
    }

    /**
     * Return the {@link Mongo} instance to connect to. Annotate with {@link Bean} in case you want to expose a
     * {@link Mongo} instance to the {@link ApplicationContext}.
     *
     * @return the {@link Mongo} instance to connect to
     * @throws Exception
     */
    @Override
    public Mongo mongo() throws Exception {
        return new MongoClient(new MongoClientURI(Constants.MONGO_CONNECTION_STRING));
    }

}
