package com.trendyol.casestudy.rest.repository;

import com.trendyol.casestudy.rest.model.Configuration;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring framework interface to query data in MongoDB.
 *
 * @author Huseyin Celal Oner
 * @since 1.0
 */
public interface ConfigurationRepository extends MongoRepository<Configuration, String> {
}
