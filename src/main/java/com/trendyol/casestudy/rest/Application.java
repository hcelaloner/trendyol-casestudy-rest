package com.trendyol.casestudy.rest;

import com.trendyol.casestudy.rest.model.Configuration;
import com.trendyol.casestudy.rest.repository.ConfigurationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application implements CommandLineRunner {

    @Autowired
    private ConfigurationRepository repository;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    /**
     * Callback used to run the bean.
     *
     * @param args incoming main method arguments
     * @throws Exception on error
     */
    @Override
    public void run(String... args) throws Exception {

        repository.deleteAll();

        // save some Configurations
        Configuration configuration1 = new Configuration(1, "SiteName", "String", "trendyol.com", 1, "SERVICE-A");
        Configuration configuration2 = new Configuration(2, "IsBasketEnabled", "Boolean", "1", 1, "SERVICE-B");
        Configuration configuration3 = new Configuration(3, "MaxItemCount", "Int", "50", 0, "SERVICE-A");
        repository.save(configuration1);
        repository.save(configuration2);
        repository.save(configuration3);

        // fetch all Configurations
        System.out.println("Configurations found with findAll():");
        System.out.println("-------------------------------");
        for (Configuration configuration : repository.findAll()) {
            System.out.println(configuration);
        }
        System.out.println();

    }

}
