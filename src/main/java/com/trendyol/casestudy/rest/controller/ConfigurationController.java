package com.trendyol.casestudy.rest.controller;

import com.trendyol.casestudy.rest.model.Configuration;
import com.trendyol.casestudy.rest.repository.ConfigurationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;


/**
 * Provides RESTful web services for WEB UI.
 */
@RestController
public class ConfigurationController {

    // MongoDB connection
    private ConfigurationRepository configurationRepository;

    @Autowired
    public ConfigurationController(ConfigurationRepository configurationRepository) {
        this.configurationRepository = configurationRepository;
    }

    // To retrieve single record by id
    @RequestMapping(value = "/configuration/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Configuration> getConfiguration(@PathVariable Long id) {
        Configuration configuration = configurationRepository.findOne(String.valueOf(id));
        // If doesn't exist
        if (configuration == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(configuration, HttpStatus.OK);
    }

    // To retrieve all records
    @RequestMapping(value = "/configuration/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Configuration>> list() {
        return new ResponseEntity<>(configurationRepository.findAll(), HttpStatus.OK);
    }

    // To add single record
    @RequestMapping(value = "/configuration/add", method = RequestMethod.POST)
    public ResponseEntity<?> add(@RequestBody Configuration configuration, UriComponentsBuilder uriComponentsBuilder) {
        // If already exists
        if (configurationRepository.exists(String.valueOf(configuration.getId()))) {
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }

        configurationRepository.save(configuration);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(uriComponentsBuilder.path("/configuration/{id}").buildAndExpand(configuration.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

    // To delete single record
    @RequestMapping(value = "/configuration/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Configuration> deleteConfiguration(@PathVariable("id") long id) {
        Configuration configuration = configurationRepository.findOne(String.valueOf(id));

        // If not exists
        if (configuration == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        configurationRepository.delete(String.valueOf(id));
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }


}
