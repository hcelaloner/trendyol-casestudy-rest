package com.trendyol.casestudy.rest.model;


import com.trendyol.casestudy.rest.Constants;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * This class represents Configuration objects that we store in MongoDB.
 *
 * @author Huseyin Celal Oner
 * @since 1.0
 */
@Document(collection = Constants.MONGO_CONFIGURATIONS_COLLECTION_NAME)
public class Configuration {

    // -----------------------------------------------------------------------------
    // Section: Instance variables
    // -----------------------------------------------------------------------------

    @Field("id")
    private long id;
    private String name;
    private String type;
    private Object value;
    private int isActive;
    private String applicationName;

    // -----------------------------------------------------------------------------
    // Section: Constructors
    // -----------------------------------------------------------------------------


    public Configuration() {
    }

    public Configuration(long id, String name, String type, Object value, int isActive, String applicationName) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.value = value;
        this.isActive = isActive;
        this.applicationName = applicationName;
    }

    public Configuration(String name, String type, Object value, int isActive, String applicationName) {
        this.name = name;
        this.type = type;
        this.value = value;
        this.isActive = isActive;
        this.applicationName = applicationName;
    }

    // -----------------------------------------------------------------------------
    // Section: Getters
    // -----------------------------------------------------------------------------

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public Object getValue() {
        return value;
    }

    public int isActive() {
        return isActive;
    }

    public String getApplicationName() {
        return applicationName;
    }

    // -----------------------------------------------------------------------------
    // Section: Other methods
    // -----------------------------------------------------------------------------

    @Override
    public String toString() {
        return getClass().getName() + "[" +
                "id=" + getId() + ", " +
                "name=" + getName() + ", " +
                "type=" + getType() + ", " +
                "value=" + getValue() + ", " +
                "isActive=" + isActive() + ", " +
                "applicationName=" + getApplicationName() + ", " +
                "]";
    }

}

