package com.trendyol.casestudy.rest;

public class Constants {
    /**
     * Connection string for mongodb instance
     */
    public static final String MONGO_CONNECTION_STRING = "mongodb://localhost:27017";

    /**
     * Name of the database to use
     */
    public static final String MONGO_DATABASE_NAME = "test";

    /**
     * Name of the collection to use
     */
    public static final String MONGO_CONFIGURATIONS_COLLECTION_NAME = "configurations";

}
